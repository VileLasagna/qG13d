import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: window
    visible: true
    width: 1300
    height: 1000
    title: qsTr("G13d Config GUI")


    header: ToolBar
    {
        id: headerBar
        //contentHeight: toolButton.implicitHeight

        RowLayout
        {
            id: toolBarLayout
            anchors.fill: parent

            ToolButton
            {
                id: toolButton
                text: stackView.depth > 1 ? "\u25C0" : "\u2630"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: drawer.position > 0.5 ? drawer.close() : drawer.open()
            }
            ListView
            {
                id: pageTabs
                orientation: ListView.Horizontal
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                highlight:  Rectangle
                            {
                                width: 180
                                height: 40
                                color: "#66a7abb2"
                                radius: 5
                                y: pageTabs.currentItem.y
                                Behavior on y {
                                    SpringAnimation {
                                        spring: 36
                                        damping: 0.2
                                    }
                                }
                            }
                highlightFollowsCurrentItem:  true
                delegate:Rectangle
                        {
                            color:"transparent"
                            height: headerBar.height
                            width: 100
                            Text
                            {
                                text:page
                                color: "#FFFFFFFF"
                                anchors.centerIn: parent
                            }
                            MouseArea
                            {
                                anchors.fill: parent
                                onClicked: pageTabs.currentIndex = index
                            }
                        }

                model:ListModel
                {
                    ListElement
                    {
                        page: "Keys"
                    }
                    ListElement
                    {
                        page: "Screen"
                    }
                    ListElement
                    {
                        page: "Macros"
                    }
                }
            }
        }

    }

    Rectangle
    {
        id:mainArea
        anchors.top:window.header.bottom
        anchors.bottom: parent.bottom
        anchors.left:parent.left
        anchors.right:parent.right
        color: "green"
        Drawer
        {

            id: drawer
            width: parent.width * 0.9
            height: parent.height
            topMargin: window.header.height


            Rectangle
            {
                color: "magenta"
                anchors.fill: parent
                FilePicker
                {
                    anchors.fill: parent
                }
            }
        }
        StackLayout
        {
            z:1
            id: stackView
            anchors.fill: parent
            currentIndex: pageTabs.currentIndex
            Item
            {
                id: keysPage
                KeysPage {}
            }
            Item
            {
                id: screen
                ScreenPage {}
            }
            Item
            {
                id: macros
                MacrosPage {}
            }
        }
    }

}
