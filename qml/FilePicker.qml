import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item
{
    SystemPalette { id: sysPallete; colorGroup: SystemPalette.Active }

    TreeView
    {
        anchors.fill: parent
            TableViewColumn
            {
                title: "Name"
                role: "fileName"
                width: 300

            }
        model: File_Model
        rootIndex: G13IO.folderIndex
        style:  TreeViewStyle
                {
//                    backgroundColor: "#3D4249"
//                    alternateBackgroundColor: "#3D4249"
//                    textColor:"#FFFFFF"
//                    highlightedTextColor: "#294570"
                    backgroundColor: sysPallete.base
                    alternateBackgroundColor: sysPallete.alternateBase
                    textColor:sysPallete.text
                    highlightedTextColor: sysPallete.highlightedText

                }
        onDoubleClicked: G13IO.loadProfile(index)
    }

}
