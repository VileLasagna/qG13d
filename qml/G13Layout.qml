import QtQuick 2.10
import QtQuick.Controls 2.3
import G13.Device 1.0


Item {

    id: g13
    property double imageScale: Math.min(deviceImage.paintedHeight / deviceImage.sourceSize.height, deviceImage.paintedWidth / deviceImage.sourceSize.width )
    property bool debugDraw: false

    property double buttonGlowRadius: 4
    property double buttonGlowThickness: 5
    property double stickBtnRadius: 20 * imageScale
    property double imageWidth: deviceImage.paintedWidth
    property int currentBtn: -1

    width: deviceImage.implicitWidth

    signal buttonClicked( var buttonID)

    function select(btn)
    {
        if(currentBtn === btn)
        {
            currentBtn = -1
        }
        else
        {
            currentBtn = btn
        }
        buttonClicked(currentBtn)
    }

    Image
    {
        id: deviceImage
        anchors.fill: parent
//        anchors.top: parent.top
//        anchors.bottom: parent.bottom
//        anchors.horizontalCenter: parent.horizontalCenter
        fillMode: Image.PreserveAspectFit
        source: "G13device.png"
    }

    Image
    {
        id: glowouterImage
        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.023
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.01
        //anchors.topMargin: deviceImage.height * 0.2
        height: sourceSize.height * (deviceImage.paintedHeight / deviceImage.sourceSize.height)
        width: sourceSize.width * (deviceImage.paintedWidth / deviceImage.sourceSize.width)
        fillMode: Image.PreserveAspectFit
        source: "pulseglow_home_gkey.png"
    }

    ///Buttons start here

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //        LED INTERFACE BUTTONS: BD, L1->L4, LIGHT_STATE
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : lcdarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: lcdrect
        property var btnid: G13.LCD
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:lcdarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "OHLOL"
            color: lcdarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 100 * imageScale
        width: 220 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth *  0
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.415
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : bdarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius* 5
        id: bdrect
        property var btnid: G13.BD
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:bdarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "BD"
            color: bdarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius * 1.5
        width: stickBtnRadius * 1.5

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.253
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.34
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : l1area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: l1rect
        property var btnid: G13.L1
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:l1area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "L1"
            color: l1area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 15 * imageScale
        width: 45 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.13
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.34
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : l2area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: l2rect
        property var btnid: G13.L2
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:l2area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "L2"
            color: l2area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 15 * imageScale
        width: 45 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.045
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.34
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : l3area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: l3rect
        property var btnid: G13.L3
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:l3area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "L3"
            color: l3area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 15 * imageScale
        width: 45 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.045
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.34
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : l4area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: l4rect
        property var btnid: G13.L4
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:l4area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "L4"
            color: l4area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 15 * imageScale
        width: 45 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.13
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.34
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : lsarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius * 5
        id: lsrect
        property var btnid: G13.LIGHT_STATE
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:lsarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "LS"
            color: lsarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius * 1.5
        width: stickBtnRadius * 1.5

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.253
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.34
    }


    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //        MACRO PROFILE BUTTONS: M1->M3 + MR
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////


    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : m1area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: m1rect
        property var btnid: G13.M1
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:m1area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "M1"
            color: m1area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 30 * imageScale
        width: 75 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.215
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.293
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : m2area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: m2rect
        property var btnid: G13.M2
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:m2area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "M2"
            color: m2area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 30 * imageScale
        width: 84 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.076
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.293
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : m3area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: m3rect
        property var btnid: G13.M3
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:m3area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "M3"
            color: m3area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 30 * imageScale
        width: 84 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.075
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.293
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : mrarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: mrrect
        property var btnid: G13.MR
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:mrarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "MR"
            color: mrarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 30 * imageScale
        width: 75 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.217
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.293
    }


    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //         ROW 1 -> G1->G7
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g1area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g1rect
        property var btnid: G13.G1
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g1area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)

        }

        Label
        {
            anchors.centerIn:parent
            text: "G1"
            color: g1area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 36 * imageScale
        width: 60 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.34
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.225
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g2area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g2rect
        property var btnid: G13.G2
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g2area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G2"
            color: g2area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 40 * imageScale
        width: 51 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.218
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.22
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g3area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g3rect
        property var btnid: G13.G3
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g3area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G3"
            color: g3area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 41 * imageScale
        width: 51 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.11
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.22
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g4area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g4rect
        property var btnid: G13.G4
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g4area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G4"
            color: g4area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 50 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.001
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.22
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g5area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g5rect
        property var btnid: G13.G5
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g5area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G5"
            color: g5area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 50 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.105
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.22
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g6area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g6rect
        property var btnid: G13.G6
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g6area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G6"
            color: g6area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 52 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.217
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.22
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g7area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g7rect
        property var btnid: G13.G7
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g7area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G7"
            color: g7area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 37 * imageScale
        width: 63 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.335
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.225
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //         ROW 2 -> G8->G14
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g8area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g8rect
        property var btnid: G13.G8
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g8area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G8"
            color: g8area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 36 * imageScale
        width: 54 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.34
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.155
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g9area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g9rect
        property var btnid: G13.G9
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g9area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G9"
            color: g9area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 52 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.228
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.15
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g10area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g10rect
        property var btnid: G13.G10
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g10area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G10"
            color: g10area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 52 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.113
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.147
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g11area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g11rect
        property var btnid: G13.G11
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g11area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G11"
            color: g11area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 44 * imageScale
        width: 50 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.001
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.145
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g12area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g12rect
        property var btnid: G13.G12
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g12area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G12"
            color: g12area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 52 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.108
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.15
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g13area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g13rect
        property var btnid: G13.G13
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g13area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G13"
            color: g13area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 54 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.222
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.152
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g14area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g14rect
        property var btnid: G13.G14
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g14area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G14"
            color: g14area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 40 * imageScale
        width: 55 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.335
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.158
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //         ROW 3 -> G15->G19
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////



    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g15area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g15rect
        property var btnid: G13.G15
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g15area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G15"
            color: g15area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 68 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.245
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.075
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g16area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g16rect
        property var btnid: G13.G16
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g16area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G16"
            color: g16area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 43 * imageScale
        width: 54 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.12
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.073
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g17area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g17rect
        property var btnid: G13.G17
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g17area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G17"
            color: g17area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 46 * imageScale
        width: 52 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.003
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.07
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g18area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g18rect
        property var btnid: G13.G18
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g18area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G18"
            color: g18area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 44 * imageScale
        width: 54 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.112
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.072
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g19area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g19rect
        property var btnid: G13.G19
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g19area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G19"
            color: g19area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 42 * imageScale
        width: 64 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.23
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * -0.075
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //         ROW 4 -> G20->G22
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////


    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g20area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g20rect
        property var btnid: G13.G20
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g20area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G20"
            color: g20area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 43 * imageScale
        width: 93 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.148
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.003
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g21area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g21rect
        property var btnid: G13.G21
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g21area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G21"
            color: g21area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 46 * imageScale
        width: 52 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * -0.003
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.003
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : g22area.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: g22rect
        property var btnid: G13.G22
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:g22area
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "G22"
            color: g22area.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 44 * imageScale
        width: 90 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.142
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.003
    }


    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //
    //         STICK AREA
    //
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////


    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : leftarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: leftrect
        property var btnid: G13.LEFT
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:leftarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: leftarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 93 * imageScale
        width: 37 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.265
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.105
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : downarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius
        id: downrect
        property var btnid: G13.DOWN
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id: downarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: downarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: 46 * imageScale
        width: 62 * imageScale

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.358
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.187
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : tophatarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius*2
        id: tophatrect
        property var btnid: G13.TOP
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:tophatarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: tophatarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius
        width: stickBtnRadius

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.4
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.086
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : stickuparea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius*2
        id: stickuprect
        property var btnid: G13.STICK_UP
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:stickuparea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: stickuparea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius
        width: stickBtnRadius

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.4
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.055
    }


    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : stickleftarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius*2
        id: stickleftrect
        property var btnid: G13.STICK_LEFT
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:stickleftarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: stickleftarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius
        width: stickBtnRadius

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.361
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.086
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : stickdownarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius*2
        id: stickdownrect
        property var btnid: G13.STICK_DOWN
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:stickdownarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: stickdownarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius
        width: stickBtnRadius

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.4
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.12
    }

    Rectangle
    {
        color: "transparent"
        border.color: debugDraw ? "yellow" : stickrightarea.containsMouse ? logitecBlue : "transparent"
        border.width: buttonGlowThickness
        radius: buttonGlowRadius*2
        id: stickrightrect
        property var btnid: G13.STICK_RIGHT
        property bool selected: g13.currentBtn === btnid
        MouseArea
        {
            id:stickrightarea
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            onClicked: g13.select(parent.btnid)
        }

        Label
        {
            anchors.centerIn:parent
            text: "."
            color: stickrightarea.containsMouse ? logitecBlue : parent.selected ? selectedColor : "white"
        }
        height: stickBtnRadius
        width: stickBtnRadius

        anchors.horizontalCenter: deviceImage.horizontalCenter
        anchors.horizontalCenterOffset: deviceImage.paintedWidth * 0.439
        anchors.verticalCenter: deviceImage.verticalCenter
        anchors.verticalCenterOffset: deviceImage.paintedHeight * 0.086
    }


}
