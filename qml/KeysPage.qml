import QtQuick 2.10
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.12
import G13.Device 1.0

Page
{
    anchors.fill: parent

    title: qsTr("Keys")


    property string backgroundColor: "#FF4F4F4F"
    property string textColour: "#FFFFFFFF"
    property string logitecBlue: "#00A7E0"
    property string selectedColor: "#00F7EA"

    property int lastButton: -1
    property string lastKeyID: "None"
    property string lastKeyTxt: "None"

    background: Rectangle { id: bg; anchors.fill: parent; color: backgroundColor }


    focus: true

    G13Layout
    {
        id: device
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: parent.left
        onButtonClicked: lastButton = buttonID
    }

    Rectangle
    {
        id: keySetArea
        color: backgroundColor
        radius: 20
        layer.enabled: true
        layer.effect:Glow
                    {
                        radius: 15
                        samples: 31
                        color: logitecBlue
                        transparentBorder: true
                    }
        anchors.left: device.right
        anchors.leftMargin: 50
        anchors.verticalCenter: parent.verticalCenter
        height: 300
        width: 350

        Label
        {
            id: currBtnTxt
            text: "Current Button"
            color: textColour
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 30
            anchors.leftMargin: 20
            font.pointSize: 16
        }

        Rectangle
        {
            id:currBtnBox
            height: 45
            width: 130
            color: backgroundColor
            radius: 5
            layer.enabled: true
            layer.effect:Glow
                        {
                            radius: 10
                            samples: 21
                            color: logitecBlue
                            transparentBorder: true
                        }
            anchors.verticalCenter: currBtnTxt.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 20
            Label
            {
                id: currBtn
                text: G13IO.buttonName(lastButton)
                anchors.centerIn: parent
                font.pointSize: 16
                color: selectedColor
                layer.enabled: true
                layer.effect:Glow
                            {
                                spread:0.2
                                radius: 5
                                samples: 11
                                color: logitecBlue
                                transparentBorder: true
                            }
            }
        }

        Label
        {
            id: currKeyTxt
            text: "Current Key"
            color: textColour
            anchors.top: currBtnTxt.bottom
            anchors.left: parent.left
            anchors.topMargin: 30
            anchors.leftMargin: 20
            font.pointSize: 16
        }

        Rectangle
        {
            id:currKeyBox
            height: 45
            width: 130
            color: backgroundColor
            radius: 5
            layer.enabled: true
            layer.effect:Glow
                        {
                            radius: 10
                            samples: 21
                            color: logitecBlue
                            transparentBorder: true
                        }
            anchors.verticalCenter: currKeyTxt.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 20

            Label
            {
                id: currKey
                text: G13IO.getCurrentBind(lastButton)
                anchors.centerIn: parent
                font.pointSize: 16
                color: selectedColor
                layer.enabled: true
                layer.effect:Glow
                            {
                                spread:0.2
                                radius: 5
                                samples: 11
                                color: logitecBlue
                                transparentBorder: true
                            }
            }
        }

        Label
        {
            id: newKeyTxt
            text: "New Key"
            color: textColour
            anchors.top: currKeyTxt.bottom
            anchors.left: parent.left
            anchors.topMargin: 30
            anchors.leftMargin: 20
            font.pointSize: 16
        }

        Rectangle
        {
            id:newKeyBox
            height: 45
            width: 130
            color: backgroundColor
            radius: 5
            layer.enabled: true
            layer.effect:Glow
                        {
                            radius: 10
                            samples: 21
                            color: logitecBlue
                            transparentBorder: true
                        }
            anchors.verticalCenter: newKeyTxt.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 20
            Keys.onPressed: {lastKeyID = event.key; lastKeyTxt=G13IO.keyName(event.key) }


            Label
            {
                id: newKey
                text: newKeyBox.activeFocus? lastKeyTxt : ""
                anchors.centerIn: parent
                font.pointSize: 16
                color: selectedColor
                layer.enabled: true
                layer.effect:Glow
                            {
                                spread:0.2
                                radius: 5
                                samples: 11
                                color: logitecBlue
                                transparentBorder: true
                            }
            }
            MouseArea
            {
                anchors.fill:parent
                onClicked: parent.focus = true
            }
        }

    }

//    Label
//    {
//        id: btnLabel
//        text: "Last \nButton \nclicked:\n" + G13IO.buttonName(lastButton) + (lastButton > 0 ? "(" + lastButton + ")" : "")
//        //font.pointSize: 48
//        anchors.top: parent.top
//        anchors.topMargin: 100
//        anchors.left: device.horizontalCenter
//        anchors.leftMargin: device.imageWidth/2
//    }

//    Label
//    {
//        text: "Last \nKey \npressed:\n" + lastKeyID + "\n With text:\n" + lastKeyTxt
//        //font.pointSize: 48
//        anchors.top: btnLabel.bottom
//        anchors.topMargin: 100
//        anchors.left: device.horizontalCenter
//        anchors.leftMargin: device.imageWidth/2
//    }

}
