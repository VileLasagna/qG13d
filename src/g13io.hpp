#ifndef G13SYSTEM_HPP
#define G13SYSTEM_HPP

#include <QObject>
#include <QFile>
#include <QFileSystemModel>

#include "g13profile.hpp"
#include "g13device.hpp"

namespace G13
{

    class IO : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(QModelIndex folderIndex READ BindsFolderIndex WRITE setIndex NOTIFY fileIndexChanged)

    public:
        IO();

        Q_INVOKABLE QFileSystemModel& FilesModel() noexcept {return filesModel;}
        Q_INVOKABLE QString keyName(Qt::Key key) const noexcept;
        Q_INVOKABLE QString buttonName(int btn) const noexcept;
        QModelIndex& BindsFolderIndex() noexcept {return bindsFolderIndex;}
        QIODevice& getDevice() noexcept {return G13dev;}
        Q_INVOKABLE QString getCurrentBind(int btn) const noexcept;

    signals:

        void commandSent();
        void fileIndexChanged();

    public slots:

        void loadProfile(QModelIndex profile);
        void sendCommand(QString command);
        void setIndex(QModelIndex& newIndex) noexcept {bindsFolderIndex = newIndex;}

    private:

        QFile G13dev;
        QFileSystemModel filesModel;
        QModelIndex bindsFolderIndex;
        G13::Profile currentProfile;

    };
}

#endif // G13SYSTEM_HPP
