#ifndef G13_DEVICE_HPP
#define G13_DEVICE_HPP

#include <map>
#include <Qt>
#include <QObject>

/**
 *  This header contains abstractions to represent the pyshical
 *  buttons, lcd and whatnot on the actual g13 device
 */

namespace G13
{
    Q_NAMESPACE
    enum class Button
    {
        // regular main buttons/keys
        // Iterating code relies on G1 being the first value
        // Do not change this, everything else is fine
        G1, G2, G3, G4, G5,
        G6, G7, G8, G9, G10,
        G11, G12, G13, G14,
        G15, G16, G17, G18,
        G19, G20, G21, G22,
        // screen/profile control area
        BD,
        L1, L2, L3, L4,
        LIGHT_STATE,
        // macro buttons
        M1, M2, M3, MR,
        //control stick area
        LEFT,
        DOWN,
        TOP, //this is pressing down the actual stick
        STICK_LEFT,
        STICK_RIGHT,
        STICK_UP,
        STICK_DOWN,
        // Not really a button, here for coding convenience
        LCD,
        // unknown/unused/mistery
        MISC_TOGGLE,
        LIGHT, LIGHT2,
        UNDEF1, UNDEF3,
        // Used for coding convenience
        // ENUM_END is relied on to be the last value
        ENUM_END, NONE=-1
    };
    Q_ENUM_NS(Button)

    /**
     * @brief Keyboard commands that can be sent to through the device
     *
     * The device and driver are a bit picky in what they understand so,
     * this map is to help translate between them
     *
     */
    static std::map<Qt::Key, QString> Keymap
    {
        //number keys
        {Qt::Key::Key_0,            "KEY_0"},
        {Qt::Key::Key_1,            "KEY_1"},
        {Qt::Key::Key_2,            "KEY_2"},
        {Qt::Key::Key_3,            "KEY_3"},
        {Qt::Key::Key_4,            "KEY_4"},
        {Qt::Key::Key_5,            "KEY_5"},
        {Qt::Key::Key_6,            "KEY_6"},
        {Qt::Key::Key_7,            "KEY_7"},
        {Qt::Key::Key_8,            "KEY_8"},
        {Qt::Key::Key_9,            "KEY_9"},
        //Alphabet keys
        {Qt::Key::Key_A,            "KEY_A"},
        {Qt::Key::Key_B,            "KEY_B"},
        {Qt::Key::Key_C,            "KEY_C"},
        {Qt::Key::Key_D,            "KEY_D"},
        {Qt::Key::Key_E,            "KEY_E"},
        {Qt::Key::Key_F,            "KEY_F"},
        {Qt::Key::Key_G,            "KEY_G"},
        {Qt::Key::Key_H,            "KEY_H"},
        {Qt::Key::Key_I,            "KEY_I"},
        {Qt::Key::Key_J,            "KEY_J"},
        {Qt::Key::Key_K,            "KEY_K"},
        {Qt::Key::Key_L,            "KEY_L"},
        {Qt::Key::Key_M,            "KEY_M"},
        {Qt::Key::Key_N,            "KEY_N"},
        {Qt::Key::Key_O,            "KEY_O"},
        {Qt::Key::Key_P,            "KEY_P"},
        {Qt::Key::Key_Q,            "KEY_Q"},
        {Qt::Key::Key_R,            "KEY_R"},
        {Qt::Key::Key_S,            "KEY_S"},
        {Qt::Key::Key_T,            "KEY_T"},
        {Qt::Key::Key_U,            "KEY_U"},
        {Qt::Key::Key_V,            "KEY_V"},
        {Qt::Key::Key_W,            "KEY_W"},
        {Qt::Key::Key_X,            "KEY_X"},
        {Qt::Key::Key_Y,            "KEY_Y"},
        {Qt::Key::Key_Z,            "KEY_Z"},
        //Function keys
        {Qt::Key::Key_Escape,       "KEY_ESC"},
        {Qt::Key::Key_F1,           "KEY_F1"},
        {Qt::Key::Key_F2,           "KEY_F2"},
        {Qt::Key::Key_F3,           "KEY_F3"},
        {Qt::Key::Key_F4,           "KEY_F4"},
        {Qt::Key::Key_F5,           "KEY_F5"},
        {Qt::Key::Key_F6,           "KEY_F6"},
        {Qt::Key::Key_F7,           "KEY_F7"},
        {Qt::Key::Key_F8,           "KEY_F8"},
        {Qt::Key::Key_F9,           "KEY_F9"},
        {Qt::Key::Key_F10,          "KEY_F10"},
        {Qt::Key::Key_F11,          "KEY_F11"},
        {Qt::Key::Key_F12,          "KEY_F12"},
        //Signs and punctuation
        {Qt::Key::Key_Apostrophe,   "KEY_APOSTROPHE"},
        {Qt::Key::Key_Agrave,       "KEY_GRAVE"},
        {Qt::Key::Key_Slash,        "KEY_SLASH"},
        {Qt::Key::Key_Backslash,    "KEY_BACKSLASH"},
        {Qt::Key::Key_Comma,        "KEY_COMMA"},
        {Qt::Key::Key_Period,       "KEY_DOT"},
        {Qt::Key::Key_Semicolon,    "KEY_SEMICOLON"},
        {Qt::Key::Key_Equal,        "KEY_EQUAL"},
        {Qt::Key::Key_BraceLeft,    "KEY_LEFTBRACE"},
        {Qt::Key::Key_BraceRight,   "KEY_RIGHTBRACE"},
        {Qt::Key::Key_Minus,        "KEY_MINUS"},
        {Qt::Key::Key_Asterisk,     "KEY_KPASTERISK"},
        {Qt::Key::Key_Plus,         "KEY_KPPLUS"},
        // Text flow and modifiers
        {Qt::Key::Key_Backspace,    "KEY_BACKSPACE"},
        {Qt::Key::Key_Space,        "KEY_SPACE"},
        {Qt::Key::Key_Tab,          "KEY_TAB"},
        {Qt::Key::Key_Return,       "KEY_ENTER"},
        {Qt::Key::Key_Alt,          "KEY_LEFTALT"},
        {Qt::Key::Key_Control,      "KEY_LEFTCTRL"},
        {Qt::Key::Key_Shift,        "KEY_LEFTSHIFT"},
        {Qt::Key::Key_AltGr,        "KEY_RIGHTALT"},
        {Qt::Key::Key_ScrollLock,   "KEY_SCROLLLOCK"},
        {Qt::Key::Key_CapsLock,     "KEY_CAPSLOCK"},
        {Qt::Key::Key_NumLock,      "KEY_NUMLOCK"},
        //Navigation and directional
        {Qt::Key::Key_Insert,       "KEY_INSERT"},
        {Qt::Key::Key_Delete,       "KEY_DELETE"},
        {Qt::Key::Key_Home,         "KEY_HOME"},
        {Qt::Key::Key_End,          "KEY_END"},
        {Qt::Key::Key_PageUp,       "KEY_PAGEUP"},
        {Qt::Key::Key_PageDown,     "KEY_PAGEDOWN"},
        {Qt::Key::Key_Right,        "KEY_RIGHT"},
        {Qt::Key::Key_Left,         "KEY_LEFT"},
        {Qt::Key::Key_Up,           "KEY_UP"},
        {Qt::Key::Key_Down,         "KEY_DOWN"}

        /**
         * The following are present from the driver side
         * but aren't differentiated within the Qt keyboard treatment
         * It's kinda dumb but there's not much I can do, so I'm just leaving
         * these here in case they suddenly become recognised in future versions
         * As I write this, I'm using Qt 5.12
         */

        /*
        {Qt::Key::Key_,"KEY_RIGHTCTRL"},
        {Qt::Key::Key_,"KEY_RIGHTSHIFT"},
        {Qt::Key::Key_,"KEY_KP0"},
        {Qt::Key::Key_,"KEY_KP1"},
        {Qt::Key::Key_,"KEY_KP2"},
        {Qt::Key::Key_,"KEY_KP3"},
        {Qt::Key::Key_,"KEY_KP4"},
        {Qt::Key::Key_,"KEY_KP5"},
        {Qt::Key::Key_,"KEY_KP6"},
        {Qt::Key::Key_,"KEY_KP7"},
        {Qt::Key::Key_,"KEY_KP8"},
        {Qt::Key::Key_,"KEY_KP9"},
        {Qt::Key::Key_,"KEY_KPDOT"},
        {Qt::Key::Key_,"KEY_KPMINUS"},
        {Qt::Key::Key_Enter,"KEY_KPENTER"}, //this the only one where it's the other way around, driver doesn't have it
        */
    };

    QString keyName(Qt::Key key) noexcept;
    Qt::Key keyFromName(QString s) noexcept;
    QString buttonName(G13::Button btn) noexcept;
    G13::Button getButton(QString s) noexcept;

}


#endif //G13_DEVICE_HPP
