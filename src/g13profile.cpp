#include "g13profile.hpp"

#include <QDebug>

G13::Profile::Profile()
    :rgb{255,255,255},
     file{""}
{}

G13::Profile::Profile(QString filepath)
    :rgb{255,255,255},
     file{filepath}
{
    LoadFrom(filepath);
}

bool G13::Profile::LoadFrom(QString filepath)
{
    QStringList lines;
    QFile f(filepath);
    if(!f.exists())
    {
        qDebug() << "G13::Profile --> Attempting to load from non-existant file: " << filepath.toLatin1();
        return false;
    }
    f.open(QFile::ReadOnly);
    if(!f.isOpen() || !f.isReadable())
    {
        qDebug() << "G13::Profile --> Could not read from file: " << filepath.toLatin1();
        return false;
    }
    while(!f.atEnd())
    {
        lines.append( QString(f.readLine()) );
    }
    f.close();

    for(QString line : lines)
    {
        auto tokens = line.split(" ");
        if(tokens.size() < 1)
        {
            //empty line
            continue;
        }
        if(tokens[0].startsWith("#"))
        {
            //this comparison is possible because split should strip whitespace
            //comment
            continue;
        }
        if(tokens[0].startsWith("rgb", Qt::CaseInsensitive))
        {
            if(tokens.length() < 4)
            {
                qDebug() << "G13::Profile --> Malformed rbg line in: " << filepath.toLatin1();
                qDebug() << "G13::Profile --> Too few arguments in line:";
                qDebug() << "G13::Profile --> " << line.toLatin1();
                qDebug() << "G13::Profile --> Skipping THIS LINE";
                continue;
            }
            else
            {
                bool r = true , g = true, b = true;
                rgb[0] = static_cast<uint8_t>(tokens[1].toUShort(&r));
                rgb[1] = static_cast<uint8_t>(tokens[2].toUShort(&g));
                rgb[2] = static_cast<uint8_t>(tokens[3].toUShort(&b));
                if(! ( r && g && b ) )
                {
                    qDebug() << "G13::Profile --> Malformed rbg line in: " << filepath.toLatin1();
                    qDebug() << "G13::Profile --> RGB values must be between 0 and 255 in line:";
                    qDebug() << "G13::Profile --> " << line.toLatin1();
                    qDebug() << "G13::Profile --> Using defaults and skipping THIS LINE";
                    rgb = {255,255,255};
                    continue;
                }
            }
            continue;
        }
        if(tokens[0].startsWith("bind", Qt::CaseInsensitive))
        {
            if(tokens.length() < 3)
            {
                qDebug() << "G13::Profile --> Malformed bind line in: " << filepath.toLatin1();
                qDebug() << "G13::Profile --> Too few arguments in line:";
                qDebug() << "G13::Profile --> " << line.toLatin1();
                qDebug() << "G13::Profile --> Skipping THIS LINE";
                continue;
            }
            binds.insert(std::pair<G13::Button, Qt::Key>(G13::getButton(tokens[1]),G13::keyFromName(tokens[2])));
        }

    }
    return true;
}

bool G13::Profile::SaveAs(QString filepath) const
{

}

QString G13::Profile::getBinding(G13::Button btn) const noexcept
{
    auto val = binds.find(btn);
    if(val == binds.end())
    {
        return "";
    }
    else
    {
        return G13::keyName(val->second);
    }
}


