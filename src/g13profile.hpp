#ifndef G13_PROFILE_HPP
#define G13_PROFILE_HPP

#include <array>
#include <cinttypes>

#include <QString>
#include <QStringList>
#include <QFile>

#include "g13device.hpp"

namespace G13
{

    class Profile
    {

    public:

        Profile();
        Profile(QString filepath);

        bool LoadFrom(QString filepath);
        bool SaveAs(QString filepath) const;


        QString getBinding(G13::Button btn)const noexcept;
        void Bind(G13::Button btn, Qt::Key key);

    private:

        std::array<uint8_t,3> rgb;
        std::map<G13::Button, Qt::Key> binds;
        QString file;

    };
}


#endif //G13_PROFILE_HPP
