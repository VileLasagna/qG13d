#include "g13device.hpp"

#include <algorithm>
#include <QDebug>

QString G13::keyName(Qt::Key key) noexcept
{
    auto item = G13::Keymap.find(key);
    if(item != Keymap.end())
    {
        return item->second;
    }
    else
    {
        qDebug() << "G13::KeyName not found";
        return "";
    }
}

QString G13::buttonName(G13::Button btn) noexcept
{
    switch(btn)
    {
        case G13::Button::G1:
            return "G1";
        case G13::Button::G2:
            return "G2";
        case G13::Button::G3:
            return "G3";
        case G13::Button::G4:
            return "G4";
        case G13::Button::G5:
            return "G5";
        case G13::Button::G6:
            return "G6";
        case G13::Button::G7:
            return "G7";
        case G13::Button::G8:
            return "G8";
        case G13::Button::G9:
            return "G9";
        case G13::Button::G10:
            return "G10";
        case G13::Button::G11:
            return "G11";
        case G13::Button::G12:
            return "G12";
        case G13::Button::G13:
            return "G13";
        case G13::Button::G14:
            return "G14";
        case G13::Button::G15:
            return "G15";
        case G13::Button::G16:
            return "G16";
        case G13::Button::G17:
            return "G17";
        case G13::Button::G18:
            return "G18";
        case G13::Button::G19:
            return "G19";
        case G13::Button::G20:
            return "G20";
        case G13::Button::G21:
            return "G21";
        case G13::Button::G22:
            return "G22";
        case G13::Button::BD:
            return "BD";
        case G13::Button::L1:
            return "L1";
        case G13::Button::L2:
            return "L2";
        case G13::Button::L3:
            return "L3";
        case G13::Button::L4:
            return "L4";
        case G13::Button::LIGHT_STATE:
            return "LIGHT_STATE";
        case G13::Button::M1:
            return "M1";
        case G13::Button::M2:
            return "M2";
        case G13::Button::M3:
            return "M3";
        case G13::Button::MR:
            return "MR";
        case G13::Button::LEFT:
            return "LEFT";
        case G13::Button::DOWN:
            return "DOWN";
        case G13::Button::TOP:
            return "TOP";
        case G13::Button::STICK_LEFT:
            return "STICK_LEFT";
        case G13::Button::STICK_RIGHT:
            return "STICK_RIGHT";
        case G13::Button::STICK_UP:
            return "STICK_UP";
        case G13::Button::STICK_DOWN:
            return "STICK_DOWN";
        case G13::Button::LCD:
            return "LCD";
        case G13::Button::MISC_TOGGLE:
            return "MISC_TOGGLE";
        case G13::Button::LIGHT:
            return "LIGHT";
        case G13::Button::LIGHT2:
            return "LIGHT2";
        case G13::Button::UNDEF1:
            return "UNDEF1";
        case G13::Button::UNDEF3:
            return "UNDEF3";
        case G13::Button::ENUM_END:
            qDebug() << "Trying to get the string for ENUM_END. This is almost certainly an error somewhere";
            [[clang::fallthrough]];
        case G13::Button::NONE:
            return "";
    }
}


G13::Button G13::getButton(QString s) noexcept
{
    // This could probably be all constexpr but I don't think
    // any of the stuff in QString is
    static std::map <QString, G13::Button> btnNameMap;
    static bool init = false;
    if(!init)
    {
        G13::Button b = G13::Button::G1;
        while(b != G13::Button::ENUM_END)
        {
            btnNameMap.insert( std::pair<QString, G13::Button>(buttonName(b),b) );
            //increment b
            b = static_cast<G13::Button>(std::underlying_type<G13::Button>::type(b) + 1);
        }
    }
    return btnNameMap.at(s);
}

Qt::Key G13::keyFromName(QString s) noexcept
{
    //remove extra characters such as \n
    s = s.trimmed();
    auto pair = std::find_if(G13::Keymap.begin(),
                             G13::Keymap.end(),
                             [s](const std::pair<Qt::Key, QString> it){return it.second.compare(s) == 0;} );

    if(pair == G13::Keymap.end())
    {
        qDebug() << "Could not find Qt::Key for Qstring " << s.toLatin1();
    }
    return pair->first;
}
