#include "g13io.hpp"

#include <QDebug>
#include <QStandardPaths>

#include "g13device.hpp"
#include "g13profile.hpp"

G13::IO::IO()
{
    //try the systemd location first
    G13dev.setFileName("/run/g13d/g13-0");
    if(!G13dev.exists() || !G13dev.open(QFile::WriteOnly))
    {
        //try the standard one
        G13dev.setFileName("/tmp/g13-0");
        if(!G13dev.exists() || !G13dev.open(QFile::WriteOnly))
        {
            qDebug() << "Cannot open G13 device";
        }
    }
    QDir BindsFolder(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation));
    if(!BindsFolder.cd("g13d"))
    {
        if(!BindsFolder.mkdir("g13d"))
        {
            qDebug() << "Cannot find nor create " << BindsFolder.absolutePath() + "/g13d";
        }
        else
        {
            BindsFolder.cd("g13d");
        }
    }


    filesModel.setRootPath("/");
    bindsFolderIndex = filesModel.index(BindsFolder.absolutePath());
}

QString G13::IO::keyName(Qt::Key key) const noexcept
{
    return G13::keyName(key);
}

QString G13::IO::buttonName(int btn) const noexcept
{
    return G13::buttonName(G13::Button(btn));
}

QString G13::IO::getCurrentBind(int btn) const noexcept
{

    return currentProfile.getBinding(G13::Button(btn));
}

void G13::IO::loadProfile(QModelIndex profile)
{
    if(!G13dev.isOpen())
    {
        qDebug() << "G13 device is not open";
        return;
    }
    QFile file(filesModel.filePath(profile));
    currentProfile.LoadFrom(filesModel.filePath(profile));
    file.open(QFile::ReadOnly);
    QByteArray line;
    do
    {
        line = file.readLine();
        if(G13dev.write(line) < 0)
        {
            qDebug() << "Error sending command to G13 device";
        }
        G13dev.flush();
    }
    while(!line.isEmpty());
}

void G13::IO::sendCommand(QString command)
{
    if(!G13dev.isOpen())
    {
        return;
    }
    else
    {
        G13dev.write(command.toLatin1());
    }
}

