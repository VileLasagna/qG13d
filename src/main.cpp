#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>

#include "g13io.hpp"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    qmlRegisterUncreatableMetaObject(
                G13::staticMetaObject,
                "G13.Device",
                1, 0,
                "G13",
                "Error: static and enums only");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    G13::IO g13s;


    engine.rootContext()->setContextProperty("File_Model", &g13s.FilesModel());
    engine.rootContext()->setContextProperty("G13IO", &g13s);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
    {
        return -1;
    }

    auto ret = app.exec();

    return ret;
}
